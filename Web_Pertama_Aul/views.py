from django.shortcuts import render
from .models import Jadwal
from .forms import Message_Form
from django.http import HttpResponseRedirect
response = {'author' : 'Aulia Ramadhani'}
# Create your views here.
def index (request):
     return render(request , '1_Beranda.html')
def profil (request):
     return render(request , '2_Profil.html')
def prestasi (request):
     return render(request , '3_Prestasi.html')
def pengalaman (request):
     return render(request , '4_Pengalaman.html')
def kontak (request):
     return render(request , '5_Kontak.html')
def registrasi (request):
     return render(request , '6_Registrasi.html')
def jadwalresult (request):
    message = Jadwal.objects.all()
    response['message'] = message
    html = '8_Jadwal_Result.html'
    return render(request, html , response)
def isijadwal (request):
	form = Message_Form (request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['kegiatan'] = request.POST['kegiatan'] 
		response['hari'] = request.POST['hari']
		response['tanggal'] = request.POST['tanggal']
		response['waktu'] = request.POST['waktu']
		response['tempat'] = request.POST['tempat']
		response['kategori'] = request.POST['kategori']
		

		jadwal = Jadwal(kegiatan=response['kegiatan'],hari=response['hari'], 
						tanggal=response['tanggal'], waktu=response['waktu'], 
						tempat=response['tempat'],kategori=response['kategori'])
		jadwal.save()
		return HttpResponseRedirect ('../isijadwal') 

	else:
		print('input salah')
	response['message_form'] = form
	html = '7_Form_Jadwal.html'
	return render(request , html , response)
def hapusjadwal(request):
	Jadwal.objects.all().delete()
	return HttpResponseRedirect ('../jadwalresult')

