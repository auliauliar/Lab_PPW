from django import forms

class Message_Form(forms.Form):
    # error_messages = {
    #     'required': 'Tolong isi input ini',
    #     'invalid': 'Isi input dengan email',
    # }

    # attrs = {
    #     'class': 'form-control',
    # }
    kegiatan = forms.CharField(label = 'Kegiatan', required=True, max_length=100 , widget = forms.TextInput(attrs = {'size' : 100 , 'class' : 'form-control'}))
    hari = forms.CharField(label='Hari', required = True, max_length=10 , widget = forms.TextInput(attrs = {'size' : 100, 'class' : 'form-control'}))
    tanggal = forms.DateField(label='Tanggal', required = True, widget = forms.DateInput(attrs = {'type' : 'date', 'class' : 'form-control'}))
    waktu = forms.TimeField(label='Waktu', required = True, widget = forms.TimeInput(attrs = {'type' : 'time', 'class' : 'form-control'}))
    tempat=  forms.CharField(label='Tempat', required = True, max_length=30 , widget = forms.TextInput(attrs = {'size' : 100 , 'class' : 'form-control'}))
    kategori= forms.CharField(label='Kategori', required = True, max_length=40 , widget = forms.TextInput(attrs = {'size' : 100 , 'class' : 'form-control'}))