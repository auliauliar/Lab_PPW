from django.conf.urls import url
from .views import index
from .views import profil
from .views import prestasi
from .views import pengalaman
from .views import kontak
from .views import registrasi
from .views import jadwalresult
from .views import isijadwal
from .views import hapusjadwal


urlpatterns = [
 url(r'^$', index, name ='index'),
 url(r'^profil/', profil, name ='profil'),
 url(r'^prestasi/', prestasi, name ='prestasi'),
 url(r'^pengalaman/', pengalaman, name ='pengalaman'),
 url(r'^kontak/', kontak, name ='kontak'),
 url(r'^registrasi/', registrasi, name ='registrasi'),
 url(r'^isijadwal/', isijadwal, name ='isijadwal'),
 url(r'^jadwalresult/', jadwalresult, name ='jadwalresult'),
 url(r'^hapusjadwal/', hapusjadwal, name ='hapusjadwal'),
]
